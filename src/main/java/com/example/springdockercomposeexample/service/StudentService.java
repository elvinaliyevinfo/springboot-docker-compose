package com.example.springdockercomposeexample.service;

import com.example.springdockercomposeexample.dto.StudentDto;
import com.example.springdockercomposeexample.dto.StudentResponse;
import com.example.springdockercomposeexample.entity.Student;
import com.example.springdockercomposeexample.mapper.StudentMapper;
import com.example.springdockercomposeexample.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    @CacheEvict(value = "students", allEntries = true)
    public StudentResponse addStudent(StudentDto studentDto) {

        Student student=studentMapper.mapDtoToEntity(studentDto);
        studentRepository.save(student);
        log.info("Successfully inserted {}",student);
        return studentMapper.mapEntityToResponse(student);
    }

    @Cacheable(cacheNames = "students")
    public List<StudentResponse> getAllStudents() {
        List<StudentResponse> students=studentRepository
                .findAll()
                .stream().map(studentMapper::mapEntityToResponse)
                .toList();

        return students;
    }

    @Cacheable(cacheNames = "student", key = "#id")
    public StudentResponse getById(Long id) {
        StudentResponse response=studentRepository
                .findById(id)
                .map(studentMapper::mapEntityToResponse)
                .orElseThrow();

        return response;
    }


    @Caching(
            evict = {@CacheEvict(value = "students", allEntries = true)},
            put = {@CachePut(value = "student", key = "#id")}
    )
    public StudentResponse updateStudent(Long id, StudentDto studentDto) {
        Student student=studentRepository
                .findById(id)
                .orElseThrow();
        student.setName(studentDto.getName());
        student.setSurname(studentDto.getSurname());
        student.setAge(studentDto.getAge());

        studentRepository.save(student);

        return studentMapper.mapEntityToResponse(student);
    }



    @Caching(
            evict = {
                    @CacheEvict(cacheNames = "student", key = "#id", beforeInvocation = true),
                    @CacheEvict(cacheNames = "students", allEntries = true)
            }
    )
    public void removeStudent(Long id) {
        studentRepository.deleteById(id);
    }
}
