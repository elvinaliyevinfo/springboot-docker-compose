package com.example.springdockercomposeexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableCaching
@EnableTransactionManagement
public class SpringDockerComposeExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDockerComposeExampleApplication.class, args);
    }

}

