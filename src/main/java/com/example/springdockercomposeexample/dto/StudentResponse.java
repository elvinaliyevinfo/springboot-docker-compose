package com.example.springdockercomposeexample.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Builder
public class StudentResponse implements Serializable {
    private static final long serialVersionUID = -4439114469417994311L;

    private Long id;
    private String name;
    private String surname;
    private int age;
}
