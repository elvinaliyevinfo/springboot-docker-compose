package com.example.springdockercomposeexample.controller;

import com.example.springdockercomposeexample.dto.StudentDto;
import com.example.springdockercomposeexample.dto.StudentResponse;
import com.example.springdockercomposeexample.entity.Student;
import com.example.springdockercomposeexample.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    StudentResponse addStudent(@RequestBody
                                    StudentDto studentDto) {

        return studentService.addStudent(studentDto);
    }

    @GetMapping
    List<StudentResponse> getAllStudents() {
        return studentService.getAllStudents();
    }

    @GetMapping("/{id}")
    StudentResponse getById(@PathVariable Long id) {
        return studentService.getById(id);
    }

    @PutMapping("/{id}")
    public StudentResponse updateStudent(@RequestBody StudentDto studentDto,
                       @PathVariable Long id) {
        return studentService.updateStudent(id, studentDto);
    }

    @DeleteMapping("/{id}")
    void updateStudent(@PathVariable Long id) {
        studentService.removeStudent(id);
    }

}



