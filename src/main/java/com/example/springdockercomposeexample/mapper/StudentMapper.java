package com.example.springdockercomposeexample.mapper;

import com.example.springdockercomposeexample.dto.StudentDto;
import com.example.springdockercomposeexample.dto.StudentResponse;
import com.example.springdockercomposeexample.entity.Student;
import org.springframework.stereotype.Component;

@Component
public class StudentMapper {

    public Student mapDtoToEntity(StudentDto studentDto){
        return Student.builder()
                .name(studentDto.getName())
                .surname(studentDto.getSurname())
                .age(studentDto.getAge())
                .build();
    }

    public StudentResponse mapEntityToResponse(Student student){
        return StudentResponse.builder()
                .id(student.getId())
                .name(student.getName())
                .surname(student.getSurname())
                .age(student.getAge())
                .build();
    }

}
